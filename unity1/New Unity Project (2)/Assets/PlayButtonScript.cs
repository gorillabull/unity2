﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayButtonScript : MonoBehaviour
{
    public GameObject panel; 
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void click()
    {
        panel.SetActive(false);
        EscapeMenuScript.GAME_PAUSED = false;
    }
}
