﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class contains information about the firepoints configuration on the npc 
/// </summary>
public class modeldata : CellScript
{
    public int firePointCount = 1;
    public int bulletArtId;

    public float[] x, y, z;
    public float[] xrot, yrot, zrot;
    public Vector3 scale;
    public float collisionScale;
    public float speed;
    public int damage; 

    public float rateOfFire, HP;
    ///sprite id from the list of sprites the cell has
    public int spriteid = 0;

    /// <summary>
    /// Instantiate a sprite of this type and sets its values to the internal data. 
    /// </summary>
    public virtual void create(GameObject cell)
    {


    }
    /// <summary>
    /// Spawns a gameobject of this type with app the properties.
    /// </summary>
    /// <param name="cell"></param>
    public void make(GameObject cell)
    {
        //REGULAR NPCS CREATION 
        Vector3 pos = cell.GetComponent<CircleCollider2D>().bounds.center;

        CellScript cll = cell.GetComponent<CellScript>();

        GameObject p = Instantiate(cll.ball, pos, cll.transform.rotation);

        p.SetActive(true);
        //set the sprite image 
        p.GetComponent<SpriteRenderer>().sprite =
          cll.sprites[spriteid].GetComponent<SpriteRenderer>().sprite;

        p.GetComponent<Enemy>().center = cll.GetComponent<Collider2D>().bounds.center;

        Enemy e = p.GetComponent<Enemy>();

        e.center = cll.GetComponent<Collider2D>().bounds.center;

        //set shot interval and hp
        //e.hp = 20;
        e.shot_interval = 50;

        for (int t = 0; t < firePointCount; t++)
        {
            e.fireptsEnabled[t] = 1;
        }

        for (int h = firePointCount; h < 10; h++)
        {
            e.fireptsEnabled[h] = 0;
        }

        //set the firepoints pos 
        for (int i = 0; i < firePointCount; i++)
        {
            e.firepoints[i] = Instantiate(
                e.FIRE_POINT_main,
              e.center, e.transform.rotation);
            e.firepoints[i].transform.parent = e.transform;         //set them as children. 

            e.firepoints[i].transform.localPosition = new Vector3(x[i], y[i]);
            e.firepoints[i].transform.rotation = Quaternion.Euler(0, 0, zrot[i]);
        }


        //set projectile art 
        e.bullet = e.bullets[bulletArtId];

        //set scale 
        if (scale.x != 0 && scale.y != 0)
        {
            e.GetComponent<Transform>().localScale = scale;
        }

        //collider scale //dont think we need to do this actually 
        if (collisionScale != 0)
        {
            e.GetComponent<CircleCollider2D>().radius = collisionScale;
        }
        //movement speed, needs work 
        if (speed != 0)
        {
            e.speedUpConf = speed;
        }
        //how often the ship fires
        if (rateOfFire != 0)
        {
            e.perShotInterval = (int)rateOfFire;
        }
        //the ships health 
        if (HP != 0)
        {
            e.hp = (int)HP;
        }

        //the ships's damage (how much points it removes)
        if (damage!=0)
        {
            e.Damage = damage;
        }
        cll.balls.Add(p);


        cellsNPCSCreated[cll.gameObject] = true;

        foreach (var item in cll.balls)
        {
            item.SetActive(true);
        }
        //  created = true;
    }
}

internal class bossShip : modeldata
{
    public int firePointCount = 3;
    public bossShip()
    {
        x = new float[10];
        y = new float[10];
        zrot = new float[10];
        //firepoint one 
        x[0] = 1.06f;
        y[0] = 2.42f;
        z[0] = 0f;
        zrot[0] = 62.336f;
        //second 
        x[1] = -1.1f;
        y[1] = -2.42f;
        z[1] = 0;
        zrot[1] = 118.127f;
        //third
        x[2] = .01f;
        y[2] = 3.05f;
        zrot[2] = 90.51f;

    }
}

/// <summary>
/// No firepoint tiny guy 1: from image
/// </summary>
internal class l1 : modeldata
{

    public void create(GameObject cell)
    {
        x = y =zrot= new float[10];
        x[0] = 1.07f;
        y[0] = -.25f;
        zrot[0] = 0f;
        firePointCount = 0;

        spriteid = 6;
        bulletArtId = 0;

        damage = 0;
        HP = 100;
        //END INIT DATA

        //REGULAR NPCS CREATION 
        make(cell);
    }
}

internal class l2 : modeldata
{

    public void create(GameObject cell)
    {
        x = y = zrot = new float[10];

        firePointCount = 0;
        x[0] = 1f;
        y[0] = 1f;
        zrot[0] = 1f;

        spriteid = 6;
        bulletArtId = 0;

        damage = 0;
        HP = 300;
        //END INIT DATA

        //REGULAR NPCS CREATION 
        make(cell);
    }
}

internal class l3 : modeldata
{

    public void create(GameObject cell) 
    {
        x = y = zrot = new float[10];

        x[0] = y[0] = zrot[0] = 1f;
        firePointCount = 0;
        spriteid = 6;
        bulletArtId = 0;

        damage = 0;
        HP = 350;

        make(cell);
    }
}

/// <summary>
/// Here there is one firepoint so xyz and rot are needed. 
/// </summary>
internal class l4 : modeldata
{

    public void create(GameObject cell)
    {
        //INIT DATA
        firePointCount = 1;
        x = new float[10];
        y = new float[10];
        zrot = new float[10];
        x[0] = 1.283f;
        y[0] = .01f;
        zrot[0] = 0f;
        spriteid = 6;
        bulletArtId = 0;

        damage = 1;
        HP = 500;
        //END INIT DATA

        //REGULAR NPCS CREATION 
        make(cell);
    }
}

internal class l5 : modeldata
{

    public void create(GameObject cell)
    {
        //INIT DATA
        firePointCount = 2;
        x = new float[10];
        y = new float[10];
        zrot = new float[10];
        x[0] = 1.383f;
        y[0] = .054f;
        zrot[0] = 0f;
        x[1] = 1.383f;
        y[1] = -0.419f;
        zrot[1] = 0;
        spriteid = 7;
        bulletArtId = 6;

        damage = 2;
        HP = 800;
        //END INIT DATA 

        make(cell);

    }
}

internal class l6 : modeldata
{
    public void create(GameObject cell)
    {
        //INIT DATA
        firePointCount = 1;
        x = new float[10];
        y = new float[10];
        zrot = new float[10];
        x[0] = 1.28f;
        y[0] = -0.16f;
        zrot[0] = 0f;
        spriteid = 8;
        bulletArtId = 2;

        damage = 4;
        HP = 1600;
        //END INIT DATA 

        make(cell);
    }
}

internal class l7 : modeldata
{
    public void create(GameObject cell)
    {
        int iter = 0;
        //INIT DATA
        firePointCount = 5;
        x = new float[10];
        y = new float[10];
        zrot = new float[10];
        x[iter] = 1.39f;
        y[iter] = 0f;
        zrot[iter] = 0f;
        iter++;
        //----------------------------------
        x[iter] = 0.9199972f;
        y[iter] = 0.2699992f;
        zrot[iter] = 29.831f;
        iter++;
        //----------------------------------
        x[iter] = 0.9500027f;
        y[iter] = -0.2800001f;
        zrot[iter] = -26.852f;
        iter++;
        //----------------------------------
        x[iter] = 0.53f;
        y[iter] = .3f;
        zrot[iter] = 29.831f;
        iter++;
        //----------------------------------
        x[iter] = 0.52f;
        y[iter] = -0.31f;
        zrot[iter] = -26.852f;
        iter++;
        //----------------------------------

        spriteid = 9;
        bulletArtId = 1;

        damage = 2;
        HP = 1200;
        //END INIT DATA 

        make(cell);
    }
}


internal class l8 : modeldata
{
    public void create(GameObject cell)
    {
        int iter = 0;
        //INIT DATA
        firePointCount = 2;
        x = new float[10];
        y = new float[10];
        zrot = new float[10];

        x[iter] = 0.37f;
        y[iter] = 0.26f;
        zrot[iter] = 0f;
        iter++;
        //----------------------------------
        x[iter] = 0.35f;
        y[iter] = -0.22f;
        zrot[iter] = 0f;
        iter++;
        //----------------------------------


        spriteid = 10;
        bulletArtId = 1;

        damage = 2;
        HP = 1800;
        //END INIT DATA 

        make(cell);
    }
}


internal class l9 : modeldata
{
    public void create(GameObject cell)
    {
        int iter = 0;
        //INIT DATA
        firePointCount = 1;
        x = new float[10];
        y = new float[10];
        zrot = new float[10];

        x[iter] = 2.37f;
        y[iter] = -0.03f;
        zrot[iter] = 0f;
        iter++;
        //----------------------------------


        spriteid = 11;
        bulletArtId = 5;

        scale = new Vector3(4.054592f, 4.868234f, 4.0223f);
        collisionScale = 1.5f;
        speed = -.05f; //slow 

        damage = 10;
        HP = 2500;
        //END INIT DATA 

        make(cell);
    }
}


public class CellScript : MonoBehaviour
{
    public static GameObject CurrentCell;
    public static Dictionary<GameObject, bool> cellsNPCSCreated;
    public static GameObject c1, c2, c3, c4;
    private bool NPCSCreated = false;

    public GameObject ball;
    public GameObject[] sprites;
    public GameObject[] bullets;

    private System.Random r;
    public List<GameObject> balls;
    private static bool created = false;
    private Dictionary<int, GameObject> frag1;
    private List<GameObject> destoyList_FragSpawn; //for stuff that spawns when npcs are hit 
    public  List<GameObject> pauseList_NPC;

    public int npcsCreated; 

    System.Type[] ty;


    /*
 * 
 * firepoint offsets for big boss ship: xyz rot  
 * 1.06, 2.42 ,0,  (0,0, 62.336)
 * -1.1, 2.42, 0 , (0,0, 118.127 )
 * .01, 3.05 , 0, (0,0, 90.51)
 */


    /*
     xp levels:
     1- 30 : 2 small guys 
     30-180: 2 small guys, 2 medium guys, 2 large guys  || any combination of them. 
     180-300: any combination of 2 large, 3 fighters, 5 something up to a certain sum 
     300-600: really big stuff here

    */
    private void Update()
    {
        if (startmovingThem == 1)
        {
            if (player!=null) //seems impossible but who knows 
            {
                foreach (var item in frag1)
                {
                    // Calculate direction vector
                    Vector3 dir = item.Value.transform.position - player.transform.position;
                    // Normalize resultant vector to unit Vector
                    dir = dir.normalized * -1;
                    // Move in the direction of the direction vector every frame 
                    item.Value.transform.position += dir * Time.deltaTime * 30;
                }

            }   
        }


        if (frag1.Count > 100)
        {
            foreach (var item in frag1)
            {
                // Calculate direction vector
                Vector3 dir = item.Value.transform.position - player.transform.position;
                // Normalize resultant vector to unit Vector
                dir = dir.normalized * -1;
                // Move in the direction of the direction vector every frame 
                item.Value.transform.position += dir * Time.deltaTime * 30;
            }

            
        }

    }

    private void Awake()
    {
        

        balls = new List<GameObject>();
        frag1 = new Dictionary<int, GameObject>();
        destoyList_FragSpawn = new List<GameObject>();
        cellsNPCSCreated = new Dictionary<GameObject, bool>();
        pauseList_NPC = new List<GameObject>();

        cellsNPCSCreated.Add(sprites[0], false);
        cellsNPCSCreated.Add(sprites[5], false);

        //types
        ty = new System.Type[10];
        ty[0] = typeof(l1);
        ty[1] = typeof(l2);


    }

    public GameObject player;
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player1"))
        {
            player = null;
            //clean up any bubbles. 
            foreach (var item in frag1)
            {
                destoyList_FragSpawn.Add(item.Value);
            }

            for (int i = 0; i < destoyList_FragSpawn.Count; i++)
            {
                Destroy(destoyList_FragSpawn[i]);
            }

            frag1.Clear();
            destoyList_FragSpawn.Clear();

            foreach (var item in pauseList_NPC)
            {
                if (item != null)
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {


        if (other.gameObject.CompareTag("Player1"))
        {
            player = other.gameObject;
            //check if player has already entered so we dont create npcs a second time
            if (!cellsNPCSCreated.ContainsKey(this.gameObject))
            {
                cellsNPCSCreated.Add(this.gameObject, true);
                //depending on which cell we are in we will create the boss ship 
                create(CharacterCOntroller2D.cumulatXp);

                Vector3.MoveTowards(other.gameObject.transform.position,
                    this.transform.position, 1000);
            }
            //unpause the npcs 
            foreach (var item in pauseList_NPC)
            {
                if (item != null)
                {
                    item.gameObject.SetActive(true);
                }
            }


        }

        //for fragments that spawn when npcs are hit 
        if (other.gameObject.CompareTag("bac1"))
        {
            if (!frag1.ContainsKey(other.GetInstanceID()))
            {
                frag1.Add(other.GetInstanceID(), other.gameObject);
            }

        }

        //when an npc is created, record it so it can be paused. 
        if (other.gameObject.CompareTag("bball2"))
        {
            pauseList_NPC.Add(other.gameObject);
            
        }

    }

    public int startmovingThem = 0;
    public void createBoss()
    {
        Vector3 pos = GetComponent<CircleCollider2D>().bounds.center;

        GameObject p =
        Instantiate(ball, pos, this.transform.rotation);
        p.SetActive(false);

        bossShip boss = new bossShip();

        Enemy e = p.GetComponent<Enemy>();
        for (int i = 0; i < 3; i++)
        {
            e.firepoints[i] = Instantiate(e.FIRE_POINT_main,
                new Vector3(boss.x[i], boss.y[i], 0), Quaternion.Euler(0, 0, boss.zrot[i]));
        }



    }
    public void create(int level)
    {
        //here check what xp level the player has , based on that create npcs, cell location does not matter, he can be in any cell 
        //this also means that npc spawn amounts per xp level is also needed to be planned. 

        if (CellSpawner.cellTags[gameObject] == "Cell2Wall")
        {
            Debug.Log("wn");
            //BOSS CREATION 

            Vector3 pos = GetComponent<CircleCollider2D>().bounds.center;

            GameObject p =
            Instantiate(ball, pos, this.transform.rotation);
            p.SetActive(false);

            p.GetComponent<SpriteRenderer>().sprite =
              sprites[7].GetComponent<SpriteRenderer>().sprite;

            Enemy e = p.GetComponent<Enemy>();

            e.center = GetComponent<Collider2D>().bounds.center;
            e.hp = 10000;
            e.shot_interval = 5;
            //set the boss's firepoints 
            bossShip boss = new bossShip();
            for (int i = 0; i < 3; i++)
            {
                e.firepoints[i] = Instantiate(e.FIRE_POINT_main,
                    new Vector3(boss.x[i], boss.y[i], 0), Quaternion.Euler(0, 0, boss.zrot[i]));
            }

            //disable some of the firepoints. 
            for (int i = 3; i < 10; i++)
            {
                e.firepoints[i].SetActive(false);
            }

            balls.Add(p);

            cellsNPCSCreated[this.gameObject] = true;

            foreach (var item in balls)
            {
                item.SetActive(true);
            }
            created = true;
        }
        else
        {
            if (level <=60)
            {
                int checksum = 0;
                int id;
                
                while (true)
                {
                    id = UnityEngine.Random.Range(1, 4);
                    if (id==1)
                    {
                        l1 l1 = new l1();
                        l1.create(this.gameObject);
                        checksum += 1;
                    }
                    if (id==2)
                    {
                        l2 l2 = new l2();
                        l2.create(this.gameObject);
                        checksum += 2;
                    }
                    if (id==3)
                    {
                        l3 l3 = new l3();
                        l3.create(this.gameObject);
                        checksum += 3;
                    }
                    if (id==4)
                    {
                        l4 l4 = new l4();
                        l4.create(this.gameObject);
                        checksum += 4;
                    }

                    npcsCreated++;

                    if (checksum>20)
                    {
                        break;
                    }
                    
                }
                 
            }

            if (level >60 && level <=300)
            {
                int checksum = 0;
                int id;

                while (true)
                {
                    id = UnityEngine.Random.Range(4, 7);
                    if (id == 4)
                    {
                        l4 l1 = new l4();
                        l1.create(this.gameObject);
                        checksum += 5;
                    }
                    if (id == 5)
                    {
                        l5 l2 = new l5();
                        l2.create(this.gameObject);
                        checksum += 11;
                    }
                    if (id == 6)
                    {
                        l6 l3 = new l6();
                        l3.create(this.gameObject);
                        checksum += 12;
                    }
                    if (id == 7)
                    {
                        l7 l4 = new l7();
                        l4.create(this.gameObject);
                        checksum += 20;
                    }

                    npcsCreated++;

                    if (checksum > 100)
                    {
                        break;
                    }

                }
            }
            if (level >300 && level <=1000)
            {
                int checksum = 0;
                int id;

                while (true)
                {
                    id = UnityEngine.Random.Range(6, 10);
                    if (id == 6)
                    {
                        l6 l1 = new l6();
                        l1.create(this.gameObject);
                        checksum += 12;
                    }
                    if (id == 7)
                    {
                        l7 l2 = new l7();
                        l2.create(this.gameObject);
                        checksum += 20;
                    }
                    if (id == 8)
                    {
                        l8 l3 = new l8();
                        l3.create(this.gameObject);
                        checksum += 40;
                    }
                    if (id == 9)
                    {
                        l9 l4 = new l9();
                        l4.create(this.gameObject);
                        checksum += 60;
                    }

                    npcsCreated++;

                    if (checksum > 1000)
                    {
                        break;
                    }

                }
            }
            //mega ultra giga 
            if (level > 1000)
            {

            }
            /*
             
            l4 l4 = new l4();
            l4.create(this.gameObject);

            l5 l5 = new l5();
            l5.create(this.gameObject);
            l9 l9 = new l9();
            l9.create(this.gameObject);*/



            //REGULAR NPCS CREATION 
            if (1 == 0)
            {
                Vector3 pos = GetComponent<CircleCollider2D>().bounds.center;

                for (int i = 0; i < Random.Range(15f, 25f); i++)
                {
                    GameObject p =
                    Instantiate(ball, pos, this.transform.rotation);
                    //p.SetActive(false);

                    p.GetComponent<SpriteRenderer>().sprite =
                      sprites[i % 5].GetComponent<SpriteRenderer>().sprite;

                    p.GetComponent<Enemy>().center = GetComponent<Collider2D>().bounds.center;
                    Enemy e = p.GetComponent<Enemy>();

                    e.center = GetComponent<Collider2D>().bounds.center;

                    //e.hp = 20;
                    e.shot_interval = 50;

                    for (int t = 0; t < 10; t++)
                    {
                        e.fireptsEnabled[t] = 1;
                    }

                    for (int h = 2; h < 10; h++)
                    {
                        e.fireptsEnabled[h] = 0;
                    }

                    balls.Add(p);
                }
                cellsNPCSCreated[this.gameObject] = true;

                foreach (var item in balls)
                {
                    item.SetActive(true);
                }
                created = true;
            }
        }

        //"small guy" creation 
        //REGULAR NPCS CREATION 


    }


}
