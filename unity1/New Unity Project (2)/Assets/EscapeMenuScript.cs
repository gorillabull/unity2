﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscapeMenuScript : MonoBehaviour
{
    public GameObject panel;
    public static bool GAME_PAUSED;
    // Start is called before the first frame update
    private void Awake()
    {
        GAME_PAUSED = true;
    }
    void Start()
    {
        panel.SetActive(false);
        GAME_PAUSED = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape ))
        {
            panel.SetActive(!panel.activeSelf);
            GAME_PAUSED = panel.activeSelf;


        }
    }
}
