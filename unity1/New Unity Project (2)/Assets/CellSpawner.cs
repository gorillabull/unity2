﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cell;
    public static Dictionary<GameObject, string> cellTags;

    void Start()
    {
        cellTags = new Dictionary<GameObject, string>();
        float cdelta = 176.5f;
        float x, y;
        x = y = 0f;
        int c = 1;
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
               GameObject p = Instantiate(cell, new Vector3(x, y, 0), Quaternion.Euler(0, 0, 0));
                //p.tag = "Cell" + c.ToString() + "Wall";
                cellTags.Add(p, "Cell" + c.ToString() + "Wall");
                c++;
                x -= cdelta;
            }
            y += cdelta;
            x = 0f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
