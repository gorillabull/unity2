﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public int hp = 500;
    public int split_damage_min = 10;
    public GameObject deathEff;
    public GameObject bac1;
    public GameObject player;
    public Transform firepoint;
    public GameObject bullet;

    public int Damage; 

    public GameObject[] bullets; 

    public AudioSource AudioSource;
    public AudioClip deathsound;

    public Rigidbody2D me;
    public float x, y;
    System.Random r = new System.Random();
    UnityEngine.Random rand;
    float t = 0;
    float R = 1.5f;

    //for bullets
    static int duration = 400;
    static int interval = 1;
    private List<Bulnode> projectiles;
    public int shot_interval = 200;
    int time_Since_last_shot = 0;
    public int perShotInterval = 0;

    public GameObject FIRE_POINT_main;

    Vector3 position;
    private Vector2 addPos;

    Collider2D myCollider;
    public GameObject mycell;
    /// <summary>
    /// represents the center of the cell the npc is currently in 
    /// </summary>
    public Vector2 center;
    public float speedUpConf=0; 

    private GameObject inWhatcell;
    public GameObject[] firepoints;
    public int[] fireptsEnabled;

    GameObject currentcell;


    // Start is called before the first frame update
    private void Awake()
    {
        projectiles = new List<Bulnode>();
        fireptsEnabled = new int[10];
    }
    void Start()
    {
        Debug.Log("enemy created");

        myCollider = GetComponent<CircleCollider2D>();
        Enemy ene = this.GetComponent<Enemy>();
       
 /*

        for (int i = 0; i < 10; i++)
        {
            firepoints[i] = Instantiate(
                FIRE_POINT_main,
               myCollider.bounds.center + new Vector3(UnityEngine.Random.Range(1, 5), UnityEngine.Random.Range(1, 5), 5),
               transform.rotation);
            firepoints[i].transform.parent = this.transform;

        }
        for (int i = 0; i < 10; i++)
        {
            firepoints[i].transform.position = new Vector3(myCollider.bounds.center.x +
                UnityEngine.Random.Range(1, 5),
                myCollider.bounds.center.y + UnityEngine.Random.Range(1, 5), 43);
        }
        */

        x = 1;
        y = 1;
        projectiles = new List<Bulnode>();
        position = new Vector3(transform.position.x, transform.position.y);


        gameObject.SetActive(true);
        //me = GetComponent<Rigidbody2D>();

    }

    public void TakeDamage(int damage)
    {
        hp -= damage;
        int fragSpawn = damage / split_damage_min;
        FragSpawn(UnityEngine.Random.Range(2, 5));


        if (hp <= 0)
        {
            AudioSource.PlayOneShot(deathsound);
            Die();
        }
    }

    private void Die()
    {
        int min, max;
        min = 12;
        max = 32;
        clearProjectiles();


        //create some bacteria around itt
        for (int i = 0; i < UnityEngine.Random.Range(min, max); i++)
        {
            Instantiate(bac1,
                new Vector3(
            gameObject.transform.position.x + r.Next(1, 10),
            gameObject.transform.position.y + r.Next(1, 10)),
            transform.rotation);
        }

        

        CellScript cs = currentcell.GetComponent<CellScript>();
        cs.npcsCreated--;

        if (cs.npcsCreated<=0)
        {
            //move all the bubbles towards the player
            cs.startmovingThem = 1;
        }

        Destroy(gameObject);

    }

    private void FragSpawn(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Instantiate(bac1,
               new Vector3(
           gameObject.transform.position.x + r.Next(1, 10),
           gameObject.transform.position.y + r.Next(1, 10)),
           transform.rotation);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!EscapeMenuScript.GAME_PAUSED)
        {
            //for rotation 
            Vector2 direction = CharacterCOntroller2D.PLAYER.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, 5f * Time.deltaTime);


            if (Vector2.Distance(center, new Vector2(transform.position.x, transform.position.y)) > 50)
            {
                addPos = Quaternion.AngleAxis(UnityEngine.Random.Range(90f, 189f)
                    , Vector3.forward) *
                    (transform.position - new Vector3(center.x, center.y));

                Vector2 norm = addPos.normalized;



                x = addPos.x / 100;
                y = addPos.y / 100;

                //actually set new positoin 
                position.x += x+ speedUpConf;
                position.y += y+ speedUpConf;

                x = norm.x / 3;
                y = norm.y / 3;
            }
            else
            {
                if (UnityEngine.Random.Range(0, 100) < 1.5f)  // a chance to move in a diff direction
                {
                    float stat1 = .05f;

                    x += UnityEngine.Random.Range(-stat1, stat1);
                    y += UnityEngine.Random.Range(-stat1, stat1);
                }

                position.x += x;
                position.y += y;
            }

            transform.position = position;



            //shooting 
            if (true)
            {
                //pl didnt shoot in last frame 
                //still check if they shot recently 
                if (time_Since_last_shot > 1)
                {
                    time_Since_last_shot = 0;

                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }
                else
                {
                    time_Since_last_shot = 0;
                    perShotInterval++;
                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }

            }


            if (projectiles != null)
            {
                for (int i = 0; i < projectiles.Count; i++)
                {
                    Bulnode temp = new Bulnode();
                    temp.p = projectiles[i].p;
                    temp.dur = projectiles[i].dur;
                    temp.dur -= interval;
                    projectiles[i] = temp;

                    if (projectiles[i].dur <= 0)
                    {
                        Destroy(projectiles[i].p);
                        projectiles.Remove(projectiles[i]);
                    }
                }
            }

            perShotInterval++;
            time_Since_last_shot++;
        }
    }

    /// <summary>
    /// Deletes all projectiles spawned 
    /// </summary>
    void clearProjectiles()
    {
        for (int i = 0; i < projectiles.Count; i++)
        {
            Destroy(projectiles[i].p);
        }
        projectiles.Clear();
    }

    private void Shoot()
    {
        for (int i = 0; i < 10; i++)
        {
            if (fireptsEnabled[i]==1)
            {
                GameObject res = Instantiate(bullet, 
                    firepoints[i].transform.position,
                 firepoints[i].transform.rotation);

                ShootMe2 shootme2 = res.GetComponent<ShootMe2>();
                shootme2.dmg = this.Damage;

                shootme2.speed = r.Next(10, 50);

                shootme2.InstantiateMe(transform.right);


                Bulnode bn = new Bulnode();
                bn.p = res;
                bn.dur = duration;
                projectiles.Add(bn);
            }
        }


    }

    void OnTriggerEnter2D(Collider2D other)
    {



        if (other.gameObject.CompareTag("Player"))
        {
            //create the sprites and set flag for created. 
        }

        if (other.gameObject.CompareTag("Cell1Wall"))
        {

            inWhatcell = other.gameObject;
        }

        if (other.gameObject.CompareTag("Cell2Wall"))
        {

            inWhatcell = other.gameObject;

        }
        if (other.gameObject.name.ToLower().Contains("cell"))
        {
            currentcell = other.gameObject;
        }

    }




}
