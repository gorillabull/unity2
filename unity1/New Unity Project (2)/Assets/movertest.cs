﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movertest : MonoBehaviour
{
    public GameObject other;

    // Start is called before the first frame update
    void Start()
    {
 
    }

    // Update is called once per frame
    void Update()
    {
        if (other != null)
        {
            // Calculate direction vector
            Vector3 dir = this.transform.position - other.transform.position;
            // Normalize resultant vector to unit Vector
            dir = dir.normalized * -1;
            // Move in the direction of the direction vector every frame 
            this.transform.position += dir * Time.deltaTime * 3;

        //    Vector3.MoveTowards(this.transform.position, other.transform.position, 10);
        }

    }
}
