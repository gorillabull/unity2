﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct PlayerShips
{
    public string name;
    public GameObject sprite;

}
public class Button1Script : MonoBehaviour
{
    public GameObject panel;
    public GameObject button1, button2, button3;
    public GameObject player_sprite;
    public Sprite ship2;
    public Image ShipSpriteForPanel;
    public Camera mainCam;

    public GameObject panel1, panel2, panel3;
    public GameObject axeship;
    [SerializeField]
    public PlayerShips[] ships;

    Dictionary<string, GameObject> shipsDictionary;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void Awake()
    {
        shipsDictionary = new Dictionary<string, GameObject>(500);
        for (int i = 0; i < ships.Length; i++)
        {
            shipsDictionary.Add(ships[i].name, ships[i].sprite);
        }
    }
    //a func call to this is registered inside the editor 
    public void dostuff(int a)
    {
        Animator a1 = panel1.GetComponent<Animator>();
        Animator a2 = panel2.GetComponent<Animator>();
        Animator a3 = panel3.GetComponent<Animator>();

        bool isopen = a1.GetBool("Open");
        a1.SetBool("Open", !isopen);

        isopen = a2.GetBool("Open");
        a2.SetBool("Open", !isopen);

        isopen = a3.GetBool("Open");
        a3.SetBool("Open", !isopen);


        CharacterCOntroller2D.PLAYER.GetComponent<SpriteRenderer>().sprite
            = ShipSpriteForPanel.sprite
            ;
        Vector3 newScale = new Vector3(
            CharacterCOntroller2D.PLAYER.transform.localScale.x * 1.2f,
            CharacterCOntroller2D.PLAYER.transform.localScale.y * 1.2f);

        CharacterCOntroller2D.PLAYER.transform.localScale = newScale;

        Button1Script bns = GetComponent<Button1Script>();

        panel.SetActive(false);
        //spawn 

        CameraController cm = mainCam.GetComponent<CameraController>();
        GameObject old = cm.player;

        cm.player = Instantiate(shipsDictionary[bns.ShipSpriteForPanel.sprite.name],
                    CharacterCOntroller2D.PLAYER.transform.position,
                    CharacterCOntroller2D.PLAYER.transform.rotation);
        CharacterCOntroller2D.PLAYER = cm.player;

        cm.player.SetActive(true);
        bns.player_sprite = cm.player;

        //copy the ship's level etc to the new one 
        CharacterCOntroller2D oldCC = old.GetComponent<CharacterCOntroller2D>();
        CharacterCOntroller2D newcc = cm.player.GetComponent<CharacterCOntroller2D>();

        newcc.xpCount = oldCC.xpCount;
        newcc.toNextLvl = oldCC.toNextLvl;
        newcc.level = oldCC.level;
        newcc.LevelsOpened.Clear();
        newcc.LevelsOpened.AddRange(oldCC.LevelsOpened);

        Destroy(old);


        EscapeMenuScript.GAME_PAUSED = false;

    }


}
