﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script used by the projectiles only from the players 
public class ShootMe : MonoBehaviour
{
    public float speed = 200f;
    public Rigidbody2D rb;
    public  AudioSource source;
    public AudioClip a;
    public ParticleSystem ps;
    public int damage; 

    Weapon weapon;

    // Start is called before the first frame update
    void Start()
    {
        //  rb.velocity = firepnt.transform.right * speed;
        ps = GetComponent<ParticleSystem>();
        ps.GetComponent<Renderer>().sortingLayerName= "backest2";

        damage = 100;
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity *= .995f;
    }


    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Enemy enemy = hitInfo.GetComponent<Enemy>();
        if (enemy != null)
        {
            if (enemy.name == "bigball" || 
                enemy.name == "bac1(Clone)" || 
                enemy.name == "bigball2(Clone)"||
                enemy.name == "bigball2")
            {
                enemy.TakeDamage(damage);
              
                source.PlayOneShot(a, 1);
                 Destroy(gameObject);

            }

    


        }
        

    }

    public void InstantiateMe(Vector3 dir)
    {
        rb.velocity = dir * speed;
    }
   
}
