﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObjectTowards2D : MonoBehaviour
{
    public float spd = 5f; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!EscapeMenuScript.GAME_PAUSED)
        {
            Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition)
            - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, spd * Time.deltaTime);

        }

    }
}
