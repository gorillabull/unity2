﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    private Vector3 offset;


    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
  
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            GetComponent<Camera>().orthographicSize++;
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            GetComponent<Camera>().orthographicSize--;
        }
        transform.position = player.transform.position + offset;
    }

     
}
