﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
public class WeaponJetShip : MonoBehaviour
{
    public Transform mainGun;
    public Transform rightFp;
    public Transform leftFp; 

    public GameObject bullet1;
    public GameObject leftRightbullet;

    static int duration = 200;
    static int interval = 1;
    private List<Bulnode> projectiles;

    int shot_interval = 13;
    int time_Since_last_shot = 0;

    int perShotInterval = 0;
    private void Awake()
    {
        projectiles = new List<Bulnode>();
    }
    private void Start()
    {

        projectiles = new List<Bulnode>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!EscapeMenuScript.GAME_PAUSED)
        {
            if (Input.GetButton("Fire1"))
            {
                //pl didnt shoot in last frame 
                //still check if they shot recently 
                if (time_Since_last_shot > 1)
                {
                    time_Since_last_shot = 0;

                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }
                else
                {
                    time_Since_last_shot = 0;
                    perShotInterval++;
                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }

            }

           
            perShotInterval++;
            time_Since_last_shot++;
        }

        List<Bulnode> modified = new List<Bulnode>();
        List<Bulnode> toremove = new List<Bulnode>();

        foreach (var item in projectiles)
        {
            Bulnode temp = new Bulnode();
            temp.p = item.p;
            temp.dur = item.dur;
            temp.dur -= interval;


            if (temp.dur <= 0)
            {
                toremove.Add(temp);
            }
            else
            {
                modified.Add(temp);
            }
        }

        projectiles.Clear();
        projectiles.AddRange(modified);
        projectiles.RemoveAll(x => toremove.Contains(x));
        //..and delete them
        for (int i = 0; i < toremove.Count; i++)
        {
            Destroy(toremove[i].p);
        }
        toremove.Clear();
        /*    for (int i = 0; i < ProjeArrSize; i++)
            {
                Bulnode temp = new Bulnode();
                temp.p = projectiles[i].p;
                temp.dur = projectiles[i].dur;
                temp.dur -= interval;
                projectiles[i] = temp;

                Debug.Log(projectiles.Count.ToString() + "size and i is " + i.ToString());
                if (projectiles[i].dur <= 0)
                {
                    projectiles.Remove(projectiles[i]);
                    Destroy(projectiles[i].p);

                }
            }*/



    }

    private void Shoot()
    {

        GameObject res = Instantiate(bullet1,mainGun.transform.position, transform.rotation);
        GameObject left = Instantiate(bullet1, leftFp.position, leftFp.rotation);
        GameObject right = Instantiate(bullet1, rightFp.position, rightFp.rotation);

        ShootMe sm = res.GetComponent<ShootMe>();
        sm.InstantiateMe(transform.right);      //make it go in the  right direction 

        ShootMe sm2 = left.GetComponent<ShootMe>();
        sm2.InstantiateMe(leftFp.right);

        ShootMe sm3 = right.GetComponent<ShootMe>();
        sm3.InstantiateMe(rightFp.right);


        Bulnode bn = new Bulnode();
        bn.p = res;
        bn.dur = duration;

        Bulnode bn1 = new Bulnode();
        bn1.p = left    ;
        bn1.dur = duration;

        Bulnode bn2 = new Bulnode();
        bn2.p = right   ;
        bn2.dur = duration;

        projectiles.Add(bn);
        projectiles.Add(bn1);
        projectiles.Add(bn2);


    }


}
