﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeaponVoid : MonoBehaviour
{
    public Transform mainGun;
    public Transform rightFp;
    public Transform leftFp;
    public Transform fp4, fp5, fp6, fp7, fp8;

    public GameObject bullet1;
    public GameObject leftRightbullet;

    static int duration = 200;
    static int interval = 1;
    private List<Bulnode> projectiles;

    int shot_interval = 13;
    int time_Since_last_shot = 0;

    int perShotInterval = 0;
    private void Awake()
    {
        projectiles = new List<Bulnode>();
    }
    private void Start()
    {

        projectiles = new List<Bulnode>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!EscapeMenuScript.GAME_PAUSED)
        {
            if (Input.GetButton("Fire1"))
            {
                //pl didnt shoot in last frame 
                //still check if they shot recently 
                if (time_Since_last_shot > 1)
                {
                    time_Since_last_shot = 0;

                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }
                else
                {
                    time_Since_last_shot = 0;
                    perShotInterval++;
                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }

            }


            perShotInterval++;
            time_Since_last_shot++;
        }

        List<Bulnode> modified = new List<Bulnode>();
        List<Bulnode> toremove = new List<Bulnode>();

        foreach (var item in projectiles)
        {
            Bulnode temp = new Bulnode();
            temp.p = item.p;
            temp.dur = item.dur;
            temp.dur -= interval;


            if (temp.dur <= 0)
            {
                toremove.Add(temp);
            }
            else
            {
                modified.Add(temp);
            }
        }

        projectiles.Clear();
        projectiles.AddRange(modified);
        projectiles.RemoveAll(x => toremove.Contains(x));
        //..and delete them
        for (int i = 0; i < toremove.Count; i++)
        {
            Destroy(toremove[i].p);
        }
        toremove.Clear();
 

    }

    private void Shoot()
    {

        GameObject res = Instantiate(bullet1, mainGun.transform.position, transform.rotation);
        GameObject left = Instantiate(bullet1, leftFp.position, leftFp.rotation);
        GameObject right = Instantiate(bullet1, rightFp.position, rightFp.rotation);
        GameObject f1 = Instantiate(bullet1, fp4.position, fp4.rotation);
        GameObject f2 = Instantiate(bullet1, fp5.position, fp5.rotation);
        GameObject f3 = Instantiate(bullet1, fp6.position, fp6.rotation);
        GameObject f4 = Instantiate(bullet1, fp7.position, fp7.rotation);
        GameObject f5 = Instantiate(bullet1, fp8.position, fp8.rotation);

        ShootMe sm = res.GetComponent<ShootMe>();
        sm.InstantiateMe(transform.right);      //make it go in the  right direction 

        ShootMe sm2 = left.GetComponent<ShootMe>();
        sm2.InstantiateMe(leftFp.right);

        ShootMe sm3 = right.GetComponent<ShootMe>();
        sm3.InstantiateMe(rightFp.right);

        ShootMe sm4 = f1.GetComponent<ShootMe>();
        sm4.InstantiateMe(fp4.right);

        ShootMe sm5 = f2.GetComponent<ShootMe>();
        sm5.InstantiateMe(fp5.right);

        ShootMe sm6 = f3.GetComponent<ShootMe>();
        sm6.InstantiateMe(fp6.right);

        ShootMe sm7 = f4.GetComponent<ShootMe>();
        sm7.InstantiateMe(fp7.right);

        ShootMe sm8 = f5.GetComponent<ShootMe>();
        sm8.InstantiateMe(fp8.right);


        Bulnode bn = new Bulnode();
        bn.p = res;
        bn.dur = duration;

        Bulnode bn1 = new Bulnode();
        bn1.p = left;
        bn1.dur = duration;

        Bulnode bn2 = new Bulnode();
        bn2.p = right;
        bn2.dur = duration;

        Bulnode bn3 = new Bulnode();
        bn3.p = f1;
        bn3.dur = duration;

        Bulnode bn4 = new Bulnode();
        bn4.p = f2;
        bn4.dur = duration;

        Bulnode bn5 = new Bulnode();
        bn5.p = f3;
        bn5.dur = duration;

        Bulnode bn6 = new Bulnode();
        bn6.p = f4;
        bn6.dur = duration;

        Bulnode bn7 = new Bulnode();
        bn7.p = f5;
        bn7.dur = duration;

        projectiles.Add(bn);
        projectiles.Add(bn1);
        projectiles.Add(bn2);
        projectiles.Add(bn3);
        projectiles.Add(bn4);
        projectiles.Add(bn5);
        projectiles.Add(bn6);
        projectiles.Add(bn7);

    }


}
