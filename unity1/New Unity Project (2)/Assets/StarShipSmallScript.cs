﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
public class StarShipSmallScript : MonoBehaviour
{
    public Transform firepoint;
    public GameObject bullet1;
    static int duration = 200;
    static int interval = 1;
    private List<Bulnode> projectiles;

    int shot_interval = 13;
    int time_Since_last_shot = 0;

    int perShotInterval = 0;
    private void Awake()
    {
        projectiles = new List<Bulnode>();
    }
    private void Start()
    {

        projectiles = new List<Bulnode>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!EscapeMenuScript.GAME_PAUSED)
        {
            if (Input.GetButton("Fire1"))
            {
                //pl didnt shoot in last frame 
                //still check if they shot recently 
                if (time_Since_last_shot > 1)
                {
                    time_Since_last_shot = 0;

                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }
                else
                {
                    time_Since_last_shot = 0;
                    perShotInterval++;
                    if (perShotInterval > shot_interval)
                    {
                        perShotInterval = 0;
                        Shoot();
                    }
                }

            }

            List<Bulnode> modified = new List<Bulnode>();
            List<Bulnode> toremove = new List<Bulnode>();

            foreach (var item in projectiles)
            {
                Bulnode temp = new Bulnode();
                temp.p = item.p;
                temp.dur = item.dur;
                temp.dur -= interval;


                if (temp.dur <= 0)
                {
                    toremove.Add(temp);
                }
                else
                {
                    modified.Add(temp);
                }
            }
            projectiles.Clear();
            projectiles.AddRange(modified);
            projectiles.RemoveAll(x => toremove.Contains(x));
            //..and delete them
            for (int i = 0; i < toremove.Count; i++)
            {
                Destroy(toremove[i].p);
            }
            toremove.Clear();


            perShotInterval++;
            time_Since_last_shot++;
        }
    }

    private void Shoot()
    {

        GameObject res = Instantiate(bullet1, transform.position, transform.rotation);
        ShootMe sm = res.GetComponent<ShootMe>();
        sm.InstantiateMe(transform.right);
        Bulnode bn = new Bulnode();
        bn.p = res;
        bn.dur = duration;
        projectiles.Add(bn);

    }


}
