﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scratchpad
{
    public class Base
    {
        public int x;
        public void basedo()
        {
            x++;
        }
    }

    public class der :Base
    {
        public void dostuff()
        {
            x = 5;
            basedo();
            Console.WriteLine(x.ToString());
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var t = System.Activator.CreateInstance(typeof(der));
            
            t.GetType().InvokeMember("basedo",
                System.Reflection.BindingFlags.InvokeMethod |
                System.Reflection.BindingFlags.Instance |
                System.Reflection.BindingFlags.Public,
                null, t, null);

            
            return;
        }
    }
}
